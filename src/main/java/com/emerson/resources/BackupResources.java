package com.emerson.resources;

import com.emerson.models.Backup;
import com.emerson.repositorys.BackupRepository;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/backup")
public class BackupResources {

    @Autowired
    private BackupRepository backupRepository;

    @RequestMapping()
    public @ResponseBody String listaBackups() {
        ObjectMapper mapper = new ObjectMapper();
        List<Backup> listaBackups = backupRepository.findAll();
        for(Backup lista : listaBackups) {
            lista.setBody("");
        }
        try {
            return mapper.writeValueAsString(listaBackups);

        }catch (JsonGenerationException e) {
            e.printStackTrace();
        }
        catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{" + "  \"mensagemRequisicao\" : \"Erro ao baixar\"" + "}";
    }

    @RequestMapping("/id")
    public String getBackupById(@RequestParam("id") Integer id) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Optional<Backup> backup = backupRepository.findById(id);
            if (!backup.isEmpty()) {
                return mapper.writeValueAsString(backup.get());
            }else {
                return "{" + "  \"mensagemRequisicao\" : \"id não encontrado\"" + "}";
            }

        }catch (JsonGenerationException e) {
            e.printStackTrace();
        }
        catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "{" + "  \"mensagemRequisicao\" : \"Erro ao baixar\"" + "}";
    }

    @PostMapping()
    public HashMap<String, Object> salvaBackup(@RequestBody @Valid String body) {
        HashMap<String, Object> map = new HashMap<>();
        Backup backup = new Backup();

        backup.setBody(body);
        backup = backupRepository.save(backup);
        map.put("id", backup.getId());
        map.put("json", backup.getBody());

        return map;
    }

    @DeleteMapping()
    public  String deleteBackup(Integer id) {
        Optional<Backup> backup = backupRepository.findById(id);
        if (backup.isEmpty()) {
            return "{" + "  \"mensagemRequisicao\" : \"id não encontrado\"" + "}";
        }
        backupRepository.delete(backup.get());
        backup.get().setMensagemRequisicao("Backup removido com sucesso");
        return backup.get().getMensagemRequisicao();
    }
}
