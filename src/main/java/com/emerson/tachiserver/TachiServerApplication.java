package com.emerson.tachiserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan(basePackages = "com.emerson.models*")
@ComponentScan(basePackages = {"com.emerson.resources"})
@EnableJpaRepositories(basePackages = {"com.emerson.repositorys"})
@EnableTransactionManagement
public class TachiServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TachiServerApplication.class, args);
	}

}
