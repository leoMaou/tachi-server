package com.emerson.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.text.SimpleDateFormat;

@Entity
public class Backup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Lob
    @Column(name = "body")
    private String body;

    @NotBlank
    @Column(name = "data")
    private String data;

    @NotBlank
    @Column(name = "hora")
    private String hora;

    @Column(name = "mensagem_requisicao")
    private String mensagemRequisicao;

    public Backup() {
        String data = "dd/MM/yyyy";
        String hora = "h:mm - a";
        java.util.Date agora = new java.util.Date();
        SimpleDateFormat formata = new SimpleDateFormat(data);
        this.data = formata.format(agora);
        formata = new SimpleDateFormat(hora);
        this.hora = formata.format(agora);
    }

    public int getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    public String getMensagemRequisicao() {
        return mensagemRequisicao;
    }

    public void setMensagemRequisicao(String mensagemRequisicao) {
        this.mensagemRequisicao = mensagemRequisicao;
    }
}
