package com.emerson.repositorys;

import com.emerson.models.Backup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BackupRepository extends JpaRepository<Backup, Integer> {
}
