## Tachi-Server

#### DESCRIÇÃO

Aplicação para servir como servidor e armazenar os backups do tachiyomi.

##### Como instalar

###### Banco de dados

Para instalar o banco por docker.
```bash
sudo docker run --name mariadb -e MYSQL_ROOT_PASSWORD=1 -p 3306:3306 -d mariadb
```
 
Criando o banco para o projeto gerar as tabelas.
```bash
sudo docker exec -it mariadb bash
mysql -u root -p
create database tachi_server;
``` 

###### Rodar Spring

./mvnw spring-boot:run


#### Fedora Silverblue (fedora-toolbox)

<p>É possivel criar um container no podman, instalar o java dentro para então executar por la, desta forma o sistema fica mais limpo<p>

```bash
toolbox create --container dev
sudo dnf install java
toolbox enter --container dev
``` 